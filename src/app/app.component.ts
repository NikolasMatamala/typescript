import { Component } from '@angular/core';
import { Circle } from './models/circle';
import { Square } from './models/square';
import { Triangle } from './models/triangle';
import { Figure } from './models/figure';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  public circle1: Figure;

  square1: Figure;

  public triangle: Figure;

  constructor() {

    this.circle1 = new Circle(5);
    console.log(this.circle1);

    this.square1 = new Square(4);
    console.log(this.square1);

    this.triangle = new Triangle(1, 1);
    console.log(this.triangle);
  }
}
