import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Figure } from 'src/app/models/figure';

@Component({
  selector: 'app-figure',
  templateUrl: './figure.component.html',
  styleUrls: ['./figure.component.scss']
})
export class FigureComponent implements OnInit {

  @Input('figure') figure: Figure;
  
  @Output('figureChange') $figureChange: EventEmitter<Figure> = new EventEmitter<Figure>();

  type: FigureType;

  constructor() { }

  ngOnInit(): void {
    console.log(this.figure)

    switch (this.figure.constructor.name.toLowerCase()) {
      case 'square': this.type = FigureType.Square; break;
      case 'circle': this.type = FigureType.Circle; break;
      case 'triangle': this.type = FigureType.Triangle; break;
    }

  }

}

enum FigureType {
  Square = 'square',
  Circle = 'circle',
  Triangle = 'triangle',
}
