export abstract class Figure {
    abstract getArea(): number;
    abstract print(): string;
}