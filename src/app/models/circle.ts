import { Figure } from './figure';

export class Circle extends Figure {

    radius: number;

    constructor(radius: number) {
        super();
        this.radius = radius;
    }

    getArea() {
        return Math.PI * (this.radius ^ 2)
    }

    print(): string {
        return `radius: ${this.radius}`;
    }

}