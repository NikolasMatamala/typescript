import { Figure } from './figure';

export class Square extends Figure {

    side: number;

    constructor(side: number){
        super();
        this.side = side;
    }

    getArea() {
        return this.side * this.side;
    }

    print(): string {
        return `side: ${this.side}`;
    }
}