import { Figure } from './figure';

export class Triangle extends Figure {

    base: number;
    height: number;

    constructor(base: number, height: number) {
        super();
        this.base = base;
        this.height = height;
    }

    getArea() {
        return (this.base * this.height) / 2;
    }

    print(): string {
        return `base: ${this.base}, height: ${this.height}`;
    }
}